Media: CNN

Provides support for CNN videos to the Embedded Media Field module,
available at http://drupal.org/project/emfield. Install that and the included
Embedded Video Field module.
