<?php
// $Id$;

/**
 * @file
 * Embedded Video Field provider file for cnn.com
 */

/**
 * Return the information about a specific provider.
 *
 * Each provider module must implement this hook to be recognized.
 *
 * @return
 *   An associative array with the following information:
 *   'provider' => The machine name, should be same as the provider filename.
 *   'name' => The provider's human readable name.
 *   'url' => The URL to the provider's main page.
 *   'settings_description' => This will be displayed above the provider
 *   information on the administration page.
 *   'supported_features' => An array of cells to be passed to a table
 *   on the administration page, under the headers of 'Feature',
 *   'Supported', and 'Notes'.
 */
function emvideo_cnn_info() {
  $features = array(
    array(t('Autoplay'), t('No'), ''),
    array(t('RSS Attachment'), t('No'), ''),
    array(t('Thumbnails'), t('Yes'), t('There is limited support for tumbnails. The downloaded images\' dimensions are 124x70.')),
    array(t('Duration'), t('No'), ''),
    array(t('Full screen mode'), t('Yes'), 'Full screen mode is not customizable'),
    //array(t('Use JW FLV Media Player'), t('Yes'), t("You may opt to use the !flvplayer to play example.com videos if it's installed on your server.", array('!flvplayer' => l(t('JW FLV Media Player'), 'http://www.longtailvideo.com/players/jw-flv-player/')))),
  );
  return array(
    'provider' => 'cnn',
    'name' => t('CNN'),
    'url' => 'http://cnn.com/',
    'settings_description' => t('These settings specifically affect videos displayed from <a href="@cnn" target="_blank">cnn.com</a>.', array('@cnn' => 'http://cnn.com')),
    'supported_features' => $features,
  );
}

/**
 * Parse the URL or embed code provided by an editor.
 *
 * @param $embed
 *   The raw URL or embed code pasted into the text field by the editor.
 * @param $field
 *   The field data for the emvideo, emaudio, etc.
 * @return
 *   If the hook returns a non-empty string, then it is assumed to have been
 *   parsed and matched by this provider. If the hook returns an array of
 *   strings, then each string is assumed to be a regex pattern, and will
 *   be checked for a match in turn. Otherwise, it is assumed there is no
 *   match.
 */
function emvideo_cnn_extract($embed, $field) {
  // the two url's that should be tested:
  // http://cnn.com/video/?/video/world/2010/09/07/shubert.belgium.headscarf.fashion.cnn
  // http://i.cdn.turner.com/cnn/.element/apps/cvp/3.0/swf/cnn_416x234_embed.swf?context=embed_edition&videoId=world/2010/09/07/shubert.belgium.headscarf.fashion.cnn"
  return array(
    '@cnn\.com/video/\?/video/([^"\& ]+)@i',
    '@i\.cdn\.turner\.com/cnn/\.element/apps/cvp/3\.0/swf/cnn_416x234_embed\.swf\?context=embed_edition&videoId=([^"\& ]+)@i',
  );
}

/**
 * Implements emfield_PROVIDER_data.
 *
 * Provides an array to be serialised and made available with $item elsewhere.
 */
function emvideo_cnn_data($field, $item) {
  // Gather info about the item's raw flash video.
  $url = 'http://cnn.com/video/?/video/' . $item['value'];
  $response = emfield_request_header('cnn', $url);

  if ($response->code == 200) {
    // Don't give the 303 path.
    $data['flash']['url'] = $url;
    $data['flash']['size'] = $response->headers['Content-Length'];
    $data['flash']['mime'] = $response->headers['Content-Type'];
  }

  return $data;
}

/**
 * Returns a link to view the original media at the provider's site.
 *
 * @param $code
 *   The unique identifier for the third party media.
 * @param $data
 *   The original data array collected for the media.
 * @return
 *   A string containing the URL to view the original media.
 */
function emvideo_cnn_embedded_link($code, $data = array()) {
  return 'http://cnn.com/video/?/video/'. $code;
}

/**
 * Implements hook emvideo_PROVIDER_video.
 * This actually displays the full/normal-sized video we want, usually on the default page view.
 *
 * @param $code
 *   the video code for the video to embed
 * @param $width
 *   the width to display the video
 * @param $height
 *   the height to display the video
 * @param $field
 *   the field info from the requesting node
 * @param $item
 *   the actual content from the field
 * @return
 *   the html of the embedded video
 */
function emvideo_cnn_video($code, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $options['item'] = $item;
  $options['node'] = $node;
  $output = theme('emvideo_cnn_flash', $code, $width, $height, $autoplay, $options);

  return $output;
}

/**
 * Implements hook_emfield_subtheme.
 */
function emvideo_cnn_emfield_subtheme() {
  return array(
    'emvideo_cnn_flash'  => array(
      'arguments' => array('embed' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL, 'options' => array()),
      'file' => 'providers/inc.inc',
      'path' => drupal_get_path('module', 'media_cnn'),
    ),
  );
}

/**
 * The theme function.
 */
function theme_emvideo_cnn_flash($code, $width, $height, $autoplay, $options) {
  $output = sprintf('<object width="%s" height="%s">', $width, $height);
  $output .= '<param name="allowfullscreen" value="true" />'; //not configurable because if false, the button for fullscreen will still be visible
  $output .= '<param name="allowscriptaccess" value="always" />';
  $output .= '<param name="wmode" value="transparent" />';
  $output .= sprintf('<param name="movie" value="http://i.cdn.turner.com/cnn/.element/apps/cvp/3.0/swf/cnn_416x234_embed.swf?context=embed_edition&videoId=%s" />', $code);
  $output .= '<param name="bgcolor" value="#000000" />';
  $output .= sprintf('<embed src="http://i.cdn.turner.com/cnn/.element/apps/cvp/3.0/swf/cnn_416x234_embed.swf?context=embed_edition&videoId=%s" type="application/x-shockwave-flash" bgcolor="#000000" allowfullscreen="false" allowscriptaccess="always" width="%s" wmode="transparent" height="%s"></embed>', $code, $width, $height);
  $output .= '</object>';

  return $output;
}

/**
 * Implements hook mvideo_PROVIDER_thumbnail.
 * Returns the external url for a thumbnail of a specific video.
 *
 * @param $field
 *   the field of the requesting node
 * @param $item
 *   the actual content of the field from the requesting node
 * @return
 *   a URL pointing to the thumbnail
 */
function emvideo_cnn_thumbnail($field, $item, $formatter, $node, $width, $height, $options = array()) {
  // Always return the larger image, since we're storing images locally.
  // http://i2.cdn.turner.com/cnn/video/us/2010/11/12/g20.obama.presser.cnn.124x70.jpg
  // http://cnn.com/video/?/video/us/2010/11/12/g20.obama.presser.cnn
  $tn = "http://i2.cdn.turner.com/cnn/video/{$item['value']}.124x70.jpg";

  return $tn;
}

/**
 * Implements hook emvideo_PROVIDER_preview.
 * this actually displays the preview-sized video we want, commonly for the teaser
 *
 * @param $code
 *   the video code for the video to embed
 * @param $width
 *   the width to display the video
 * @param $height
 *   the height to display the video
 * @param $field
 *   the field info from the requesting node
 * @param $item
 *   the actual content from the field
 * @return
 *   the html of the embedded video
 */
function emvideo_cnn_preview($code, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $options['item'] = $item;
  $options['node'] = $node;
  $output = theme('emvideo_cnn_flash', $code, $width, $height, $autoplay, $options);

  return $output;
}
